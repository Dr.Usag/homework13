﻿#include <iostream>
#include <string>
#include <time.h>
#include "Helpers.h"

using std::cout;
using std::endl;

// Function for Homerwork15.
void PrintNumbers(const int Count, const bool IsEven)
{
    // Solution 1
    int StartPoint{ 0 };

    if(!IsEven) {
        StartPoint = 1;
    }   

    for (int i = StartPoint; i <= Count; i+=2)
    {
        cout << i << ' ';
    }

    // Solution 2
    //for (int i = 0; i <= Count; ++i)
    //{
    //    if (IsEven) {
    //        !(i % 2) ? cout << i << ' ' 
    //                 : cout << "";
    //    } 
    //    else {
    //        i % 2 ? cout << i << ' ' 
    //              : cout << "";
    //    }
    //}

    // Solution 3

    //for (int i = 0; i <= Count; ++i)
    //{
    //   if (IsEven == i % 2) {
    //        cout << i << ' ';
    //    }
    //}
    cout << endl;
}

int main()
{
    {
        /* Homework 13
         * 1) Cоздать файл заголовков Helpers.h и сохранить его в той же папке,
         *    что и главный исполняемый файл
         * 2) В Helpers.h создать функцию, которая будет принимать в качестве
         *    аргумента два числа и возвращать квадрат их суммы.
         * 3) Подключить Helpers.h с помощью #include
         * 4) Вызвать в функции main написанную ранее функцию из Helpers.h
         * 5) Поставить точку останова в теле функции, описанной в Helpers.h
        */
        Sqrt(4, 2);
    }

    {
        /* Homework 14
         * 1) В главном исполняемом файле (файл в котором находится функция main)
         *    создать и инициализировать переменную типа std::string любым значением.
         * 2) Вывести саму строковую переменную, вывести длину строки, вывести первый
         *    и последний символы этой строки. Для вывода использовать std::cout <<
        */
        cout << "Homerwork 14\n\n";

        const std::string Str{ "It's a string!" };

        cout << "The length of " << "\"" << Str << "\" = "
             << Str.length() << "\n";
        cout << "The first char is '" << Str.front() << "' "
             << "and the last is '" << Str.back() << endl << endl;       
    }

    {
        /* Homework 15
         * Написать функцию, которая в зависимости от своих параметров печатает в консоль либо четные,
         * либо нечетные числа от 0 до N (N тоже сделать параметром функции).
         * Минимизировать количество циклов и условий.
        */
        cout << "Homework 15\n\n";

        const int NumbersCount = 10;

        cout << "Only even numbers:\n";
        PrintNumbers(NumbersCount, true);
        cout << "\nOnly not even numbers:\n";
        PrintNumbers(NumbersCount, false);

        cout << endl;
    }

    {
        /* Homerwork 16
         * 1) В главном исполняемом файле (файл в котором находится функция main) создать двумерный массив
         *    размерности NxN и заполнить его таким образом, чтобы элемент с индексами i и j был равен i + j.
         * 2) Вывести этот массив в консоль.
         * 3) Вывести сумму элементов в строке массива, индекс которой равен остатку деления текущего
         *    числа календаря на N. (в двумерном массиве a[i][j], i — индекс строки).
         */
        cout << "Homework 16\n\n";

        const int Size{ 5 };

        // Init
        int Array[Size][Size];
        for (int i = 0; i < Size; ++i) {
            for (int j = 0; j < Size; ++j) {
                Array[i][j] = i + j;
            }
        }
        
        // Print
        for (int i = 0; i < Size; ++i) {
            for (int j = 0; j < Size; ++j) {
                cout << Array[i][j] << ' ';
            }
            cout << endl;
        }
        cout << endl;

        time_t Now = time(nullptr);
        struct tm Buf;
        localtime_s(&Buf, &Now);

        const int Index = Buf.tm_mday % Size;
        int Sum{ 0 };

        for (int i = 0; i < Size; ++i) {
            Sum += Array[Index][i];
        }
        cout << "Today is " << Buf.tm_mday << "th\n"
             << "Sum for index(" << Index << ") = "  << Sum << endl;
    }
    return 0;
}

